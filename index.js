const got = require("./data-1");

function isObject(obj) {
  if (Array.isArray(obj) || obj == null || typeof obj !== "object") {
    return false
  }
  return true
}

function countAllPeople(got) {
  let result = []
  if (isObject(got)) {
    result = got.houses.reduce((acc, curr) => {
      acc += curr.people.length;
      return acc;
    }, 0);
    return result;
  }
}

// console.log(countAllPeople(got));

function peopleByHouses(got) {
  if (isObject(got)) {
    got.houses.forEach((each) => {
      console.log(
        `${each.name} has ${each.people.length} memeber in thier house`
      );
    });
  }
}

//  peopleByHouses(got)

function everyone(got) {

  if (isObject(got)) {
    let res = got.houses.reduce((acc, curr) => {
      curr.people.forEach((each) => {
        acc.push(each.name);
      });
      return acc;
    }, []);
    return res;
  }

}

// console.log(everyone(got));

function nameWithS(got) {
  if (isObject(got)) {
    let res = got.houses.reduce((acc, curr) => {
      curr.people.forEach((each) => {
        if (each.name.includes("s") || each.name.includes("S")) {
          acc.push(each.name);
        }
      });
      return acc;
    }, []);
    return res;
  }
}

// console.log(nameWithS(got));

function nameWithA(got) {
  if (isObject(got)) {
    let res = got.houses.reduce((acc, curr) => {
      curr.people.forEach((each) => {
        if (each.name.includes("a") || each.name.includes("A")) {
          acc.push(each.name);
        }
      });
      return acc;
    }, []);
    return res;
  }
}

// (console.log(nameWithA(got)))

function surnameWithS(got) {
  if (isObject(got)) {
    let res = got.houses.reduce((acc, curr) => {
      curr.people.forEach((each) => {
        let surname = each.name.split(" ")[1];
        if (surname.startsWith("S")) {
          acc.push(each.name);
        }
      });
      return acc;
    }, []);
    return res;
  }
}
// console.log(surnameWithS(got));

function surnameWithA(got) {
  if (isObject(got)) {
    let res = got.houses.reduce((acc, curr) => {
      curr.people.forEach((each) => {
        let surname = each.name.split(" ")[1];
        if (surname.startsWith("A")) {
          acc.push(each.name);
        }
      });
      return acc;
    }, []);
    return res;
  }
}
// console.log(surnameWithA(got));

function peopleNameOfAllHouses(got) {
  if (isObject(got)) {
    let res = got.houses.reduce((acc, curr) => {
      curr.people.forEach (each => {
        if (!acc[curr.name]) {
          acc[curr.name] = [each.name]
        } else {
          acc[curr.name] = [...acc[curr.name], each.name]
        }
      })
  
      return acc
    }, {})
    return res
  }
}

console.log(peopleNameOfAllHouses(got));